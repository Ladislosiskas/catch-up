import { timeSlots, lessons as schedule } from "./constants.js";
const key = "lessons";
const defaultStoreValue = [];
const form = document.querySelector("#formLessons");
const loginJSON = localStorage.getItem("login");
const loginObj = JSON.parse(loginJSON);
let lessons = {};
let lessonsArray = [];
let storageLessons = localStorage.getItem(key);

form.addEventListener("submit", (event) => {
  event.preventDefault();

  const checkedType = form.querySelector('input[name="type"]:checked');
  const checkedTime = form.querySelector('input[name="time"]:checked');
  const timeId = checkedTime.getAttribute("id");
  const typeId = checkedType.getAttribute("id");

  lessons.time = timeSlots[timeId];
  lessons.title = schedule[typeId].title;
  lessons.duration = schedule[typeId].duration;
  lessons.name = loginObj.name;

  if (timeId === "time_04" || timeId === "time_05" || timeId === "time_06") {
    lessons.tomorrow = true;
  } else {
    lessons.tomorrow = false;
  }

  if (!storageLessons) {
    localStorage.setItem(key, JSON.stringify(defaultStoreValue));
    storageLessons = defaultStoreValue;
  }

  lessonsArray = JSON.parse(storageLessons);
  lessonsArray.push(lessons);

  localStorage.setItem(key, JSON.stringify(lessonsArray));

  form.reset();
});
